package com.frach.minecraft.unmute.helpers;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;
import java.util.Optional;

public abstract class MasterPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        this.enable();
        super.onEnable();
    }

    @Override
    public void onDisable() {
        this.disable();
        super.onDisable();
    }

    @Override
    public void onLoad() {
        this.load();
        super.onLoad();
    }

    public abstract void enable();
    public abstract void disable();
    public abstract void load();

    public <T> T getService(Class<T> service) {
        Objects.requireNonNull(service, "clazz");

        return Optional
                .ofNullable(Bukkit.getServicesManager().getRegistration(service))
                .map(RegisteredServiceProvider::getProvider)
                .orElseThrow(() -> new IllegalStateException("No registration present for service '" + service.getName() + "'"));
    }

    public <T> T provideService(Class<T> clazz, T instance, ServicePriority priority) {
        Objects.requireNonNull(clazz, "clazz");
        Objects.requireNonNull(instance, "instance");
        Objects.requireNonNull(priority, "priority");

        Bukkit.getServicesManager().register(clazz, instance, this, priority);

        return instance;
    }

    public <T> T provideService(Class<T> clazz, T instance) {
        provideService(clazz, instance, ServicePriority.Normal);
        return instance;
    }

    public void register(Plugin plugin, Listener... listeners){
        for (Listener listener : listeners){
            plugin.getServer().getPluginManager().registerEvents(listener,plugin);
        }
    }

    public void register(Plugin plugin, Command... commands){
        for (Command command : commands){
            ((CraftServer)plugin.getServer()).getCommandMap().register(plugin.getName().toLowerCase(),command);
        }
    }

}

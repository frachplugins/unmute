package com.frach.minecraft.unmute.helpers;

import com.google.common.collect.Lists;
import org.bukkit.entity.EntityType;
import org.bukkit.plugin.*;
import org.bukkit.configuration.file.*;
import java.io.*;
import org.bukkit.configuration.*;
import org.bukkit.*;
import java.util.*;

public class MasterFile {

    private File file;
    private FileConfiguration fileConfiguration;
    String fileName;

    public MasterFile(final Plugin plugin, String fileName) {
        this.fileName = fileName;
        if (!plugin.getDataFolder().exists()) {
            plugin.getDataFolder().mkdirs();
        }
        if (!fileName.isEmpty()) {
            fileName = (fileName.endsWith(".yml") ? fileName : (fileName + ".yml"));
        }
        this.file = new File(plugin.getDataFolder(), fileName.isEmpty() ? "config.yml" : fileName);
        if (!this.file.exists()) {
            plugin.saveResource(fileName, false);
        }
        this.fileConfiguration = (FileConfiguration)YamlConfiguration.loadConfiguration(this.file);
    }

    public void createNewFile() throws IOException {
        this.file.createNewFile();
    }

    public Object get(final String path) {
        return this.fileConfiguration.get(path);
    }

    public String getString(final String path) {
        return ChatColor.translateAlternateColorCodes('&', this.fileConfiguration.getString(path));
    }

    public int getInt(final String path) {
        return this.fileConfiguration.getInt(path);
    }

    public List<?> getList(final String path) {
        return (List<?>)this.fileConfiguration.getList(path);
    }

    public List<String> getStringList(final String path) {
        List<String> list = Lists.newArrayList();
        for (String s : this.fileConfiguration.getStringList(path)) {
            list.add(ChatColor.translateAlternateColorCodes('&', s));
        }
        return list;
    }

    public EntityType getEntityType(final String path){
        return EntityType.valueOf(getString(path));
    }

    public double getDouble(final String path) {
        return this.fileConfiguration.getDouble(path);
    }

    public float getFloat(final String path) {
        return (float)this.fileConfiguration.getDouble(path);
    }

    public List<Integer> getIntegetList(final String path) {
        return (List<Integer>)this.fileConfiguration.getIntegerList(path);
    }

    public List<Double> getDoubleList(final String path) {
        return (List<Double>)this.fileConfiguration.getDoubleList(path);
    }

    public List<Float> getFloatList(final String path) {
        return (List<Float>)this.fileConfiguration.getFloatList(path);
    }

    public boolean getBoolean(final String path) {
        return this.fileConfiguration.getBoolean(path);
    }

    public void set(final String path, final Object value) {
        this.fileConfiguration.set(path, value);
    }

    public ConfigurationSection getConfigurationSection(final String path) {
        return this.fileConfiguration.getConfigurationSection(path);
    }

    public boolean contains(final String path) {
        return this.get(path) != null;
    }

    public void save() {
        try {
            this.fileConfiguration.save(this.file);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
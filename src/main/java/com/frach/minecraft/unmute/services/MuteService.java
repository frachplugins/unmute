package com.frach.minecraft.unmute.services;

import com.frach.minecraft.unmute.data.MutePlayer;
import org.bukkit.entity.Player;

import java.util.Optional;
import java.util.UUID;

public interface MuteService {

    default MutePlayer create(Player player, int duration) {
        return this.create(player.getUniqueId(), duration);
    }

    MutePlayer create(UUID uuid, int duration);

    default void remove(Player player) {
        this.remove(player.getUniqueId());
    }

    void remove(UUID uuid);

    default Optional<MutePlayer> get(Player player) {
        return this.get(player.getUniqueId());
    }

    Optional<MutePlayer> get(UUID uuid);

    void load();
    void save();
    void task();

}
package com.frach.minecraft.unmute.services.impl;

import com.frach.minecraft.unmute.Main;
import com.frach.minecraft.unmute.data.MutePlayer;
import com.frach.minecraft.unmute.helpers.MasterFile;
import com.frach.minecraft.unmute.services.MuteService;
import com.frach.minecraft.unmute.utils.TimeParser;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashSet;
import java.util.Optional;
import java.util.UUID;

public class MuteServiceImpl implements MuteService {

    private HashSet<MutePlayer> mutePlayers = new HashSet<MutePlayer>();

    private MasterFile file;

    @Override
    public MutePlayer create(UUID uuid, int duration) {
        MutePlayer mutePlayer = new MutePlayer(uuid, duration);

        Player player = mutePlayer.getPlayer();

        if(player != null && player.isOnline()) {
            player.sendMessage(ChatColor.RED + "you have been silenced for " + TimeParser.parseLong(duration * 1000, true));
        }

        mutePlayers.add(mutePlayer);
        return mutePlayer;
    }

    @Override
    public void remove(UUID uuid) {
        Optional<MutePlayer> optionalMutePlayer = this.get(uuid);

        if(optionalMutePlayer.isPresent()){
            MutePlayer mutePlayer = optionalMutePlayer.get();

            Player player = mutePlayer.getPlayer();

            if(player != null && player.isOnline()){
                player.sendMessage(ChatColor.RED + "you are no longer silenced.");
            }

            mutePlayers.remove(mutePlayer);
        }
    }

    @Override
    public Optional<MutePlayer> get(UUID uuid) {
        return mutePlayers.stream().filter(mutePlayer -> mutePlayer.getUuid() == uuid).findFirst();
    }

    @Override
    public void load() {
        this.file = new MasterFile(Main.getInstance(), "mutes.yml");

        ConfigurationSection section = this.file.getConfigurationSection("");
        if(section != null) {
            section.getKeys(false).forEach(data -> {
                MuteServiceImpl.this.create(
                        UUID.fromString(data),
                        this.file.getInt(data + ".duration")
                );
            });
        }

        this.task();

        Main.getInstance().getLogger().info("has loaded " + mutePlayers.size() + " silences from file.");
    }

    @Override
    public void save() {
        this.mutePlayers.forEach(data -> {
            if(data.isMuted()) {
                this.file.set(data.getUuid().toString() + ".duration", (data.getTimeLeft() / 1000));
            } else {
                this.file.set(data.getUuid().toString(), null);
            }
        });
        this.file.save();
    }

    @Override
    public void task() {
        new BukkitRunnable() {
            @Override
            public void run() {
                MuteServiceImpl.this.mutePlayers.forEach(data -> {
                    if(!data.isMuted()) {
                        MuteServiceImpl.this.remove(data.getUuid());
                    }
                });
            }
        }.runTaskTimer(Main.getInstance(), 20L, 20L);
    }

}
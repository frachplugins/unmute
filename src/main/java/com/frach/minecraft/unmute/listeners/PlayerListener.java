package com.frach.minecraft.unmute.listeners;

import com.frach.minecraft.unmute.Main;
import com.frach.minecraft.unmute.data.MutePlayer;
import com.frach.minecraft.unmute.services.MuteService;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.Optional;

public class PlayerListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();

        MuteService service = Main.getInstance().getService(MuteService.class);

        Optional<MutePlayer> optionalMutePlayer = service.get(player);

        if(optionalMutePlayer.isPresent()) {
            MutePlayer mutePlayer = optionalMutePlayer.get();
            if(mutePlayer.isMuted() && !player.hasPermission("mute.bypass")) {
                event.setCancelled(true);

                player.sendMessage(ChatColor.RED + "you can only chat in " + mutePlayer.getTimeString());
            }
        }
    }

}

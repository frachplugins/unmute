package com.frach.minecraft.unmute;

import com.frach.minecraft.unmute.commands.MuteCommand;
import com.frach.minecraft.unmute.commands.UnmuteCommand;
import com.frach.minecraft.unmute.helpers.MasterPlugin;
import com.frach.minecraft.unmute.listeners.PlayerListener;
import com.frach.minecraft.unmute.services.MuteService;
import com.frach.minecraft.unmute.services.impl.MuteServiceImpl;

public final class Main extends MasterPlugin {

    private static Main instance;

    public Main() {
        Main.this.instance = this;

        provideService(MuteService.class, new MuteServiceImpl());
    }

    @Override
    public void enable() {
        getLogger().info("Plugin successfully enabled.");

        getService(MuteService.class).load();

        getCommand("mute").setExecutor(new MuteCommand());
        getCommand("unmute").setExecutor(new UnmuteCommand());
        getServer().getPluginManager().registerEvents(new PlayerListener(), this);
    }

    @Override
    public void disable() {
        getLogger().info("Plugin successfully disabled.");

        getService(MuteService.class).save();
    }

    @Override
    public void load() {}

    public static Main getInstance() {
        return instance;
    }

}
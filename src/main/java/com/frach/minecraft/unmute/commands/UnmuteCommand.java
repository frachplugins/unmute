package com.frach.minecraft.unmute.commands;

import com.frach.minecraft.unmute.Main;
import com.frach.minecraft.unmute.data.MutePlayer;
import com.frach.minecraft.unmute.services.MuteService;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Optional;

public class UnmuteCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] arguments) {
        if(arguments.length < 1 || arguments.length > 1){
            sender.sendMessage(ChatColor.DARK_RED + "please, execute " + ChatColor.RED + "/unmute <player>");
            return true;
        }
        if(!sender.hasPermission("mute.admin")){
            sender.sendMessage(ChatColor.RED + "you are not allowed to executed that command.");
            return true;
        }
        Player target = Bukkit.getPlayer(arguments[0]);

        MuteService service = Main.getInstance().getService(MuteService.class);

        Optional<MutePlayer> optionalMutePlayer = service.get(target);

        if(optionalMutePlayer.isPresent()) {
            service.remove(target);
            sender.sendMessage(ChatColor.GREEN + "player is no longer silenced.");
        } else {
            sender.sendMessage(ChatColor.RED + "this player is not muted.");
        }
        return false;
    }

}

package com.frach.minecraft.unmute.commands;

import com.frach.minecraft.unmute.Main;
import com.frach.minecraft.unmute.services.MuteService;
import com.frach.minecraft.unmute.utils.TimeParser;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MuteCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] arguments) {
        if(arguments.length < 1 || arguments.length > 2) {
            sender.sendMessage(ChatColor.DARK_RED + "please, execute " + ChatColor.RED + "/mute <player> <duration (seconds)>");
            return true;
        }
        if(!sender.hasPermission("mute.admin")) {
            sender.sendMessage(ChatColor.RED + "you are not allowed to executed that command.");
            return true;
        }
        Player target = Bukkit.getPlayer(arguments[0]);

        if(target == null) {
            sender.sendMessage(ChatColor.RED + "this player could not be found.");
            return true;
        }
        int duration = (int) TimeParser.parseString(arguments[1]) / 1000;

        Main.getInstance().getService(MuteService.class).create(target, duration);

        sender.sendMessage(ChatColor.GREEN + "successfully silenced.");

        return false;
    }

}
package com.frach.minecraft.unmute.data;

import com.frach.minecraft.unmute.utils.TimeParser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public class MutePlayer {

    private UUID uuid;
    private int duration;

    private long initialMillis;

    public MutePlayer(UUID uuid, int duration) {
        this.uuid = uuid;
        this.duration = duration;

        this.initialMillis = System.currentTimeMillis();
    }

    public UUID getUuid() {
        return uuid;
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(this.uuid);
    }

    public int getDuration() {
        return duration;
    }

    public long getInitialMillis() {
        return initialMillis;
    }

    public double getTimeLeft() {
        return ((this.duration * 1000) + this.initialMillis) - System.currentTimeMillis();
    }

    public String getTimeString() {
        String time = TimeParser.parseLong((long) getTimeLeft(), true);
        return time == null ? "now" : time;
    }

    public boolean isMuted() {
        return (this.getTimeLeft() / 1000) > 0.0;
    }

}